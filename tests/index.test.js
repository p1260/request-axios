const { postDataApi, putDataApi, getDataApi } = require("../index");

const _server = "https://reqres.in/";

describe("axios", () => {
  test("get api", async () => {
    const _resp = {
      data: {
        id: 2,
        email: "janet.weaver@reqres.in",
        first_name: "Janet",
        last_name: "Weaver",
        avatar: "https://reqres.in/img/faces/2-image.jpg"
      },
      support: {
        url: "https://reqres.in/#support-heading",
        text: "To keep ReqRes free, contributions towards server costs are appreciated!"
      }
    };

    const _req = await getDataApi(_server, "api/users/2", {});

    expect(_req).toEqual(_resp);
  });

  test("post api", async () => {
    const _user = { email: "eve.holt@reqres.in", password: "cityslicka" };

    const _req = await postDataApi(_server, "api/login", _user, {});

    expect(_req).toEqual({ token: "QpwL5tke4Pnpja7X4" });
  });

  test("put api", async () => {
    const _user = { name: "morpheus", job: "zion resident" };
   
    const _req = await putDataApi(_server, "api/users/2", _user, {});

    expect(_req.name).toEqual(_user.name);
    expect(_req.job).toEqual(_user.job);
  });
});
