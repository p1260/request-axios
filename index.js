const axios = require("axios");

module.exports = {
  async postDataApi(url, path, body, headers) {
    return await axios
      .post(`${url}${path}`, body, { headers: headers })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return {
          Retorno: -1,
          Mensaje: "No se puede obtener la información solicitada.",
          Objeto: error.response.data
        };
      });
  },
  async putDataApi(url, path, body, headers) {
    return await axios
      .put(`${url}${path}`, body, { headers: headers })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return {
          Retorno: -1,
          Mensaje: "No se puede obtener la información solicitada.",
          Objeto: error.response.data
        };
      });
  },
  async getDataApi(url, path, body, headers) {
    return await axios
      .get(`${url}${path}`, { headers: headers })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return {
          Retorno: -1,
          Mensaje: "No se puede obtener la información solicitada.",
          Objeto: error.response.data
        };
      });
  }
};
